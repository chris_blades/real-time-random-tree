Meteor.methods({
    // create 'generations' many numbers within the range of the parentNode
    runGenerator: function (parentNodeId, generations) {
        var parentNode = ParentNodes.find({_id: parentNodeId}).fetch()[0];
        
        if (parentNode && isNumeric(generations) 
            && generations > 0 && generations <= 15) {
            
            var numbers = [];
            
            var min = parentNode.rangeStart;

            var max = parentNode.rangeEnd;

            for (var i = 0; i < generations; i++) {
                var number = 
                    Math.floor(Math.random() * (max - min)) + min;

                numbers.push(number);
            }

            ParentNodes.update(parentNode._id, {$set: {numbers: numbers}});
        }
    },
    
    createParentNode: function (parentNode) {
        ParentNodes.insert(parentNode);
    },

    deleteParentNode: function (_id) {
        ParentNodes.remove(_id);
    }
});


// helper since jquery isn't available on backend
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
