// Global document ready.  
// register contextMenus
$(function () {

    // hacky, but peculiaritues of jquery context menu plugin necessitate it...
    var generations;

    // register right-click context menu
    $.contextMenu({
        selector: 'div > ul > .parentNode',
        callback: function (key, opt) {
        },
        items: {
            generate: {
                name: 'Numbers:',
                type: 'text',
                events: {
                    keyup: function(e) {
                        if ($.isNumeric(e.currentTarget.value)) {
                            generations = e.currentTarget.value;
                        }
                    }
                }
            },
            runGeneration: {
                name: 'Generate Numbers',
                callback: function(key, opt) {
                    if ($.isNumeric(generations)) {
                        Meteor.call('runGenerator', opt.$trigger.attr("id"), generations);
                    }
                }
            },
            seperator: "-----",
            remove: {
                name: "Remove Factory",
                callback: function(key, opt) {
                    Meteor.call('deleteParentNode', opt.$trigger.attr("id"));
                }
            }

        }
    });
});

