Template.userSignIn.events({
    'click #loginButton': function () {
        $('#loginButton').prop('disabled', true);        
        
        var nameError = $('#nameError');      
        var passwordError = $('#passwordError');

        nameError.hide();
        passwordError.hide();        

        var nameInput = $.trim($('.nameInput').val());
        var passwordInput = $.trim($('.passwordInput').val());
        
        if (nameInput && passwordInput) {
            Meteor.loginWithPassword(nameInput, passwordInput, function (error) {
                if (error) {
                    Accounts.createUser({username: nameInput, password: passwordInput} 
                    , function (error) {
                        if (error) {
                            console.log("Couldn't create account " + error);                        
                        } else {
                            Meteor.loginWithPassword(nameInput, passwordInput);
                        }
                    });               
                }
            });
        }

        if (!nameInput) {
            nameError.show();
        }
        
        if (!passwordInput) {
            passwordError.show();
        }

        $('#loginButton').prop('disabled', false);        
    }
});
