Template.addNodeForm.events({
    'click #addNodeButton': function () {
        $('#addNodeButton').prop('disabled', true);        
        
        var nameError = $('#nodeNameError');      
        var rangeStartError = $('#rangeStartError');
        var rangeEndError = $('#rangeStartError');

        nameError.hide();
        rangeStartError.hide();        
        rangeEndError.hide();        

        var nameInput = $.trim($('.nodeNameInput').val());
        var rangeStartInput = $.trim($('.rangeStartInput').val());
        var rangeEndInput = $.trim($('.rangeEndInput').val());
        
        var noBlanks = nameInput && rangeStartInput && rangeEndInput;
        var rangeNumeric = $.isNumeric(rangeStartInput) && $.isNumeric(rangeEndInput);

        if (noBlanks && rangeNumeric) {
            var newNode = {
                name: nameInput,
                rangeStart: parseInt(rangeStartInput),
                rangeEnd: parseInt(rangeEndInput),
                user: Meteor.user(),
                numbers: [],
                dateCreated: Date.now()
            };
        
            Meteor.call('createParentNode', newNode);
        }

        if (!nameInput) {
            nameError.show();
        }
        
        if (!rangeStartInput || !$.isNumeric(rangeStartInput)) {
            rangeStartError.show();
        }

        if (!rangeEndInput || !$.isNumeric(rangeEndInput)) {
            rangeEndError.show();
        }

        $('#addNodeButton').prop('disabled', false);        
    }
});
